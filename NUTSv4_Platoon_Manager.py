#/usr/bin/env python
import sys
import model_ui
from controller_platoon import Platoon
from operator import itemgetter


if __name__ == "__main__":

    debug = 0
    pm = Platoon(debug)
    menu_dict = {'Generate New Platoon': pm.generate_new_platoon, 'Update Existing Platoon':  pm.update_existing_platoon}
    menu = model_ui.MenuManagement()
    menu_item = menu.menu_ui(list(map(itemgetter(0), menu_dict.items())))
    menu_dict[menu_item]()

    input("Press any key to exit")
    sys.exit(1)


