from model_platoon import PlatoonManagement
from model_files import FileManagement
import model_ui
import os

class Platoon:

    def __init__(self, debug=0):
        self.debug = debug
        self.country_code   = None
        self.country_name   = None
        self.platoon        = []
        self.infantry_type  = None
        self.menu = model_ui.MenuManagement()
        self.country_code_dict = {
            'Britian': 'br',
            'Germany': 'ge',
            'Russia':  'ru',
            'United States': 'us',
        }
        print("NUTSv4 Platoon Manager\n")
 


    def set_country_code(self):
        print("The following countrys are available: ")
        country_code_list = []
        for key in self.country_code_dict.keys():
            country_code_list.append(key)
        self.country_name = self.menu.menu_ui(country_code_list)
        self.country_code = self.country_code_dict[self.country_name]
        self.fm = FileManagement(self.country_code, self.debug)
        self.pm = PlatoonManagement(self.country_code, self.debug)





    def set_unit_type(self):
        squad_attribute_map = self.fm.load_yaml_map('squad_map')
        print("The following infantry types are available: ")
        squad_type_list = []
        for attr in squad_attribute_map:
            squad_type_list.append(attr)
        self.infantry_type = self.menu.menu_ui(squad_type_list)
        self.platoon = self.pm.generate_platoon(self.infantry_type)
        self.fm.write_platoon_file(self.platoon, self.infantry_type)



    def update_existing_platoon(self):
        self.set_country_code()
        print(f"Update existing platoon for {self.country_name} \n")
        self.update_member_attr()


    def generate_new_platoon(self):
        print(f"Generate new platoon for {self.country_name} \n")
        self.set_country_code()
        self.set_unit_type()
        if self.debug:
            print("DEBUG: Platoon class platoon variable value: %s\n" % self.platoon)
        return

    def set_platoon(self):
        infantry_type_dict = self.fm.get_platoon_directories()
        infantry_type_list = []
        for key in infantry_type_dict.keys():
            infantry_type_list.append(key)
        self.infantry_type = self.menu.menu_ui(infantry_type_list)
        self.platoon = self.fm.get_platoon_map(infantry_type_dict[self.infantry_type])

    def get_squad(self):
        squad_map_list = []
        for squad in self.platoon:
            for key in squad:
                squad_map_list.append(key)
        squad_map_key = self.menu.menu_ui(squad_map_list)
        squad_index = squad_map_list.index(squad_map_key)
        squad_map = self.platoon[squad_index][squad_map_key]
        return(squad_map, squad_index, squad_map_key)

    def get_member(self, squad_map, squad_index, squad_map_key):
        member_list = []
        for member in squad_map:
            member_list.append(member['role'] + ": " + member['name'])
        member = self.menu.menu_ui(member_list)
        member_index = member_list.index(member)
        return(self.platoon[squad_index][squad_map_key][member_index], member_index)


    def update_member_attr(self):
        self.set_platoon()
        (squad_map, squad_index, squad_map_key) = self.get_squad()
        (member_map, member_index) = self.get_member(squad_map, squad_index, squad_map_key)
        attribute_list = []
        for attribute in member_map.keys():
            attribute_list.append(attribute)
        update_attribute = self.menu.menu_ui(attribute_list)
        self.debug = 1
        if self.debug:
            print(f"platoon_map member: {self.platoon[squad_index][squad_map_key][member_index]}")
        self.platoon[squad_index][squad_map_key][member_index][update_attribute] = self.menu.update_member_attribute(update_attribute)
        if self.debug:
            print(f"platoon_map member attribute: {self.platoon[squad_index][squad_map_key][member_index][attribute]}")
            print(f"platoon_map member: {self.platoon[squad_index][squad_map_key][member_index]}")
        self.fm.write_platoon_file(self.platoon, self.infantry_type)


    def get_highest_rep(self):
        self.set_platoon()
        (squad_map, squad_index, squad_map_key) = self.get_squad()
        return(self.pm.get_highest_rep(self.platoon[squad_index][squad_map_key]))